﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Scramble
{
    class Decode
    {
        public static void Main(string[] args)
        {
            byte[] graphics = GetRomGraphics();

            byte[] levelsData = GetLevelsData(Constants.DATA_START_INDEX, Constants.DATA_END_INDEX);

            ProcessSpriteRom(graphics);

            ProcessCharacterRom(graphics);

            ProcessLevelData(levelsData);

            WriteLevelsRawData(levelsData);
        }

        private static byte[] GetRomGraphics()
        {
            /* graphics bytes */
            byte[] graphicsPart1 = File.ReadAllBytes(Constants.GraphicsPart1Path);

            byte[] graphicsPart2 = File.ReadAllBytes(Constants.GraphicsPart2Path);

            byte[] graphics = new byte[graphicsPart1.Length + graphicsPart2.Length];

            System.Buffer.BlockCopy(graphicsPart1, 0, graphics, 0, graphicsPart1.Length);

            System.Buffer.BlockCopy(graphicsPart2, 0, graphics, graphicsPart1.Length, graphicsPart2.Length);

            return graphics;
        }

        private static byte[] GetLevelsData(int startIdx, int endIdx)
        {
            /* build data bytes */
            using (MemoryStream dataBuilder = new MemoryStream())
            {
                foreach (string pData in Constants.DataPath)
                {
                    byte[] data = File.ReadAllBytes(pData);
                    dataBuilder.Write(data, 0, data.Length);
                }

                byte[] rangedData = new byte[endIdx - startIdx];

                Array.Copy(dataBuilder.ToArray(), startIdx, rangedData, 0, endIdx - startIdx);

                return rangedData;
            }

        }

        private static void ProcessSpriteRom(byte[] sprite)
        {
            //static const gfx_layout scramble_spritelayout =
            //{
            //    16,16, /* 16*16 sprites */
            //    RGN_FRAC(1,2), */ 64 sprites */
            //    2,/* 2 bits per pixel */
            //    { RGN_FRAC(0,2), RGN_FRAC(1,2) }, /* the bitplanes are separated */
            //    { 0, 1, 2, 3, 4, 5, 6, 7,
            //        8*8+0, 8*8+1, 8*8+2, 8*8+3, 8*8+4, 8*8+5, 8*8+6, 8*8+7 },
            //    { 0*8, 1*8, 2*8, 3*8, 4*8, 5*8, 6*8, 7*8,
            //        16*8, 17*8, 18*8, 19*8, 20*8, 21*8, 22*8, 23*8 },
            //    32*8 /* every sprite takes 32 consecutive bytes */
            //};

            int bpp = 1;
            int[] planeOffsets = { 0, sprite.Length / 2 };

            int[] xOffsets = {   8*8+0, 8*8+1, 8*8+2, 8*8+3, 8*8+4, 8*8+5, 8*8+6, 8*8+7,
                                     0, 1, 2, 3,  4, 5, 6, 7                                
                             };
            int[] yOffsets = {   0*8, 1*8, 2*8, 3*8, 4*8, 5*8, 6*8, 7*8,
                                     16*8, 17*8, 18*8, 19*8, 20*8, 21*8, 22*8, 23*8
                             };


            using (Bitmap bm = new Bitmap((sprite.Length / 2 / (((Constants.SpriteCharSize * Constants.SpriteCharSize) / 8) * bpp) * Constants.SpriteCharSize), (Constants.SpriteCharSize * Constants.FULL_PALETTE.Length) / Constants.PALETTE_BLOCK_SIZE))
            {
                for (int k = 0; k < Constants.FULL_PALETTE.Length / Constants.PALETTE_BLOCK_SIZE; k++)
                {
                    Color[] rowColor = GetPaletteColors(k);
                    for (int i = 0; i < (sprite.Length / 2 / (((Constants.SpriteCharSize * Constants.SpriteCharSize) / 8) * bpp)); i++)
                    {
                        for (int y = 0; y < Constants.SpriteCharSize; y++)
                        {
                            for (int x = 0; x < Constants.SpriteCharSize; x++)
                            {
                                int b = 0;
                                for (int p = 0; p < planeOffsets.Length; p++)
                                {
                                    int z = sprite[(i * (((Constants.SpriteCharSize * Constants.SpriteCharSize) / 8) * bpp)) + (xOffsets[x] / 8) + (yOffsets[y] / 8) + (planeOffsets[p])] & (1 << ((xOffsets[x] % 8)));
                                    b = (b << 1) | ((z != 0) ? 1 : 0);
                                } //for    
                                if (b != 0)
                                {
                                    bm.SetPixel(((Constants.SpriteCharSize - 1) - y) + (i * Constants.SpriteCharSize), ((Constants.SpriteCharSize - 1) - x) + k * Constants.SpriteCharSize, rowColor[b]);
                                }
                            } //for                            
                        } //for
                    } //for
                }
                bm.Save(string.Format("{0}.sprite.png", Constants.Scramble), ImageFormat.Png);
            }
        }

        private static void ProcessCharacterRom(byte[] characters)
        {
            //static const gfx_layout scramble_charlayout =
            //{
            //    8,8,
            //    RGN_FRAC(1,2),
            //    2,
            //    { RGN_FRAC(0,2), RGN_FRAC(1,2) },
            //    { 0, 1, 2, 3, 4, 5, 6, 7 },
            //    { 0*8, 1*8, 2*8, 3*8, 4*8, 5*8, 6*8, 7*8 },
            //    8*8
            //};

            int bpp = 1;
            int[] planeOffsets = { 0, characters.Length / 2 };
            int[] xOffsets = { 0, 1, 2, 3, 4, 5, 6, 7 };
            int[] yOffsets = { 0 * 8, 1 * 8, 2 * 8, 3 * 8, 4 * 8, 5 * 8, 6 * 8, 7 * 8 };

            using (Bitmap bm = new Bitmap((characters.Length / 2 / (((Constants.CharactersCharSize * Constants.CharactersCharSize) / 8) * bpp) * Constants.CharactersCharSize), (Constants.CharactersCharSize * Constants.FULL_PALETTE.Length) / Constants.PALETTE_BLOCK_SIZE))
            {
                for (int k = 0; k < Constants.FULL_PALETTE.Length / Constants.PALETTE_BLOCK_SIZE; k++)
                {
                    Color[] rowColor = GetPaletteColors(k);
                    for (int i = 0; i < (characters.Length / 2 / (((Constants.CharactersCharSize * Constants.CharactersCharSize) / 8) * bpp)); i++)
                    {
                        for (int y = 0; y < Constants.CharactersCharSize; y++)
                        {
                            for (int x = 0; x < Constants.CharactersCharSize; x++)
                            {
                                int b = 0;
                                for (int p = 0; p < planeOffsets.Length; p++)
                                {
                                    int z = characters[(i * (((Constants.CharactersCharSize * Constants.CharactersCharSize) / 8) * bpp)) + (xOffsets[x] / 8) + (yOffsets[y] / 8) + (planeOffsets[p])] & (1 << ((xOffsets[x] % 8)));
                                    b = (b << 1) | ((z != 0) ? 1 : 0);
                                } //for    
                                if (b != 0)
                                {
                                    bm.SetPixel(((Constants.CharactersCharSize - 1) - y) + (i * Constants.CharactersCharSize), ((Constants.CharactersCharSize - 1) - x) + k * Constants.CharactersCharSize, rowColor[b]);
                                } //if
                            } //for                            
                        } //for                        
                    } //for                    
                }
                bm.Save(string.Format("{0}.char.png", Constants.Scramble), ImageFormat.Png);
            }
        }

        private static void ProcessLevelData(byte[] data)
        {
            Bitmap cBm = new Bitmap(Constants.CharactersPath);
            Bitmap spriteBM = new Bitmap(Constants.SpritePath);
            Bitmap lBm = new Bitmap(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
            int zone = 1;
            int idx = 0;
            int currentLevelXPos = 0;
            int currentSkipPosition = 0;
            int contentTile;
            bool isUpPainting = false;

            int[] levelsSkipInterval = new int[] { 2, 4, 2, 2, 4, 2 };

            using (Graphics gr = Graphics.FromImage(lBm))
            {
                gr.FillRectangle(new SolidBrush(Color.Black), new Rectangle() { X = 0, Y = 0, Width = lBm.Width, Height = lBm.Height });

                do
                {
                    switch (zone)
                    {
                        case 1:

                            for (idx = 0; data[idx] != 0xFF; idx += 2)
                            {
                                int yPos = data[idx] & 0xF8;
                                int tilePos = data[idx + 1] * 8;
                                if (levelsSkipInterval[zone - 1] != currentSkipPosition)
                                {
                                    DrawLevelTile(cBm, gr, tilePos, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos);
                                    DrawLevelTileContent(zone, cBm, gr, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos, currentSkipPosition, isUpPainting);
                                    currentLevelXPos += Constants.CharactersCharSize;
                                    currentSkipPosition++;
                                }
                                else
                                {
                                    switch (data[idx + 1])
                                    {
                                        case 1:
                                            //rocket
                                            DrawEnemy(spriteBM, gr, Constants.ROCKETS, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1]] - Constants.SpriteCharSize);
                                            break;
                                        case 2:
                                            //fuel
                                            DrawEnemy(spriteBM, gr, Constants.FUEL, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1]] - Constants.SpriteCharSize);
                                            break;
                                        case 4:
                                            //mystery
                                            DrawEnemy(spriteBM, gr, Constants.DROID, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1]] - Constants.SpriteCharSize);
                                            break;
                                        default:
                                            //00 empty!
                                            break;
                                    }
                                    currentSkipPosition = 0;
                                }
                            }
                            //finish: change to zone 2                            
                            //skip 0xFF (end of level tag)
                            currentSkipPosition = 0;
                            isUpPainting = false;
                            zone = 2;
                            break;
                        case 2:
                            for (idx += 1; data[idx] != 0xFF; idx += 2)
                            {
                                int yPos = data[idx] & 0xF8;
                                int tilePos = data[idx + 1] * 8;
                                if (currentSkipPosition == 2)
                                {
                                    currentLevelXPos -= (2 * Constants.CharactersCharSize);
                                    isUpPainting = true;
                                }
                                if (levelsSkipInterval[zone - 1] != currentSkipPosition)
                                {

                                    contentTile = GetTitleContentColor(zone, false, currentSkipPosition);
                                    DrawLevelTile(cBm, gr, tilePos, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos);
                                    DrawLevelTileContent(zone, cBm, gr, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos, currentSkipPosition, isUpPainting);
                                    currentLevelXPos += Constants.CharactersCharSize;
                                    currentSkipPosition++;
                                }
                                else
                                {

                                    idx--; //only skip one
                                    currentSkipPosition = 0;
                                    isUpPainting = false;

                                    switch (data[idx + 1])
                                    {
                                        case 1:
                                            //rocket
                                            DrawEnemy(spriteBM, gr, Constants.ROCKETS, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1] - 1] - Constants.SpriteCharSize);
                                            break;
                                        case 2:
                                            //fuel
                                            DrawEnemy(spriteBM, gr, Constants.FUEL, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1] - 1] - Constants.SpriteCharSize);
                                            break;
                                        case 4:
                                            //mystery
                                            DrawEnemy(spriteBM, gr, Constants.DROID, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1] - 1] - Constants.SpriteCharSize);
                                            break;
                                        default:
                                            //00 empty!
                                            break;
                                    }

                                }

                            }
                            isUpPainting = false;
                            currentSkipPosition = 0;
                            zone = 3;
                            break;
                        case 3:

                            for (idx += 1; data[idx] != 0xFF; idx += 2)
                            {
                                if (levelsSkipInterval[zone - 1] != currentSkipPosition)
                                {
                                    int yPos = data[idx] & 0xF8;
                                    int tilePos = data[idx + 1] * 8;
                                    DrawLevelTile(cBm, gr, tilePos, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos);
                                    DrawLevelTileContent(zone, cBm, gr, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos, currentSkipPosition, isUpPainting);
                                    currentLevelXPos += Constants.CharactersCharSize;
                                    currentSkipPosition++;
                                }
                                else
                                {
                                    switch (data[idx + 1])
                                    {
                                        case 1:
                                            //rocket
                                            DrawEnemy(spriteBM, gr, Constants.ROCKETS, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone]] - Constants.SpriteCharSize);
                                            break;
                                        case 2:
                                            //fuel
                                            DrawEnemy(spriteBM, gr, Constants.FUEL, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone]] - Constants.SpriteCharSize);
                                            break;
                                        case 4:
                                            //mystery
                                            DrawEnemy(spriteBM, gr, Constants.DROID, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone]] - Constants.SpriteCharSize);
                                            break;
                                        default:
                                            //00 empty!
                                            break;
                                    }
                                    currentSkipPosition = 0;
                                }
                            }
                            currentSkipPosition = 0;
                            //finish: change to zone 4
                            isUpPainting = false;
                            zone = 4;
                            break;
                        case 4:

                            for (idx += 1; data[idx] != 0xFF; idx += 2)
                            {
                                if (levelsSkipInterval[zone - 1] != currentSkipPosition)
                                {
                                    int yPos = data[idx] & 0xF8;
                                    int tilePos = data[idx + 1] * 8;
                                    DrawLevelTile(cBm, gr, tilePos, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos);
                                    DrawLevelTileContent(zone, cBm, gr, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos, currentSkipPosition, isUpPainting);
                                    currentLevelXPos += Constants.CharactersCharSize;
                                    currentSkipPosition++;
                                }
                                else
                                {
                                    switch (data[idx + 1])
                                    {
                                        case 1:
                                            //rocket
                                            DrawEnemy(spriteBM, gr, Constants.ROCKETS, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone]] - Constants.SpriteCharSize);
                                            break;
                                        case 2:
                                            //fuel
                                            DrawEnemy(spriteBM, gr, Constants.FUEL, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone]] - Constants.SpriteCharSize);
                                            break;
                                        case 4:
                                            //mystery
                                            DrawEnemy(spriteBM, gr, Constants.DROID, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone]] - Constants.SpriteCharSize);
                                            break;
                                        default:
                                            //00 empty!
                                            break;
                                    }
                                    currentSkipPosition = 0;
                                }
                            }
                            currentSkipPosition = 0;
                            isUpPainting = false;
                            //finish: change to zone 5
                            zone = 5;
                            break;

                        case 5:
                            for (idx += 1; data[idx] != 0xFF; idx += 2)
                            {
                                if (currentSkipPosition == 2)
                                {
                                    currentLevelXPos -= (2 * Constants.CharactersCharSize);
                                    isUpPainting = true;
                                }
                                if (levelsSkipInterval[zone - 1] != currentSkipPosition)
                                {
                                    int yPos = data[idx] & 0xF8;
                                    int tilePos = data[idx + 1] * 8;
                                    DrawLevelTile(cBm, gr, tilePos, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos);
                                    DrawLevelTileContent(zone, cBm, gr, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos, currentSkipPosition, isUpPainting);
                                    currentLevelXPos += Constants.CharactersCharSize;
                                    currentSkipPosition++;
                                }
                                else
                                {
                                    idx--; //only skip one
                                    currentSkipPosition = 0;
                                    isUpPainting = false;
                                    switch (data[idx + 1])
                                    {
                                        case 1:
                                            //rocket
                                            DrawEnemy(spriteBM, gr, Constants.ROCKETS, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1] - 1] - Constants.SpriteCharSize);
                                            break;
                                        case 2:
                                            //fuel
                                            DrawEnemy(spriteBM, gr, Constants.FUEL, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1] - 1] - Constants.SpriteCharSize);
                                            break;
                                        case 4:
                                            //mystery
                                            DrawEnemy(spriteBM, gr, Constants.DROID, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1] - 1] - Constants.SpriteCharSize);
                                            break;
                                        default:
                                            //00 empty!
                                            break;
                                    }
                                }
                            }
                            currentSkipPosition = 0;
                            zone = 6;
                            break;
                        case 6:
                            for (idx += 1; data[idx] != 0xFF && idx < data.Length - 2; idx += 2)
                            {
                                if (levelsSkipInterval[zone - 1] != currentSkipPosition)
                                {
                                    int yPos = data[idx] & 0xF8;
                                    int tilePos = data[idx + 1] * 8;
                                    DrawLevelTile(cBm, gr, tilePos, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos);
                                    DrawLevelTileContent(zone, cBm, gr, Constants.CHARACTER_PALETTE_INDEX, currentLevelXPos, yPos, currentSkipPosition, isUpPainting);
                                    currentLevelXPos += Constants.CharactersCharSize;
                                    currentSkipPosition++;
                                }
                                else
                                {
                                    switch (data[idx + 1])
                                    {
                                        case 1:
                                            //rocket
                                            DrawEnemy(spriteBM, gr, Constants.ROCKETS, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1]] - Constants.SpriteCharSize);
                                            break;
                                        case 2:
                                            //fuel
                                            DrawEnemy(spriteBM, gr, Constants.FUEL, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1]] - Constants.SpriteCharSize);
                                            break;
                                        case 4:
                                            //mystery
                                            DrawEnemy(spriteBM, gr, Constants.DROID, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1]] - Constants.SpriteCharSize);
                                            break;
                                        case 8:
                                            // end of game object
                                            DrawEnemy(spriteBM, gr, Constants.END_OBJECT, Constants.SPRITE_PALETTE_INDEX, currentLevelXPos - Constants.SpriteCharSize, data[idx - levelsSkipInterval[zone - 1]] - Constants.SpriteCharSize);
                                            break;
                                        default:
                                            //00 empty!
                                            break;
                                    }
                                    currentSkipPosition = 0;
                                }
                            }
                            //skip 0xFF (end of level tag)
                            isUpPainting = false;
                            currentSkipPosition = 0;
                            zone = -1;
                            break;
                    }
                } while (zone != -1);
            }

            cBm.Dispose();
            lBm.Save(string.Format("{0}.levels.png", Constants.Scramble), ImageFormat.Png);
            lBm.Dispose();
        }

        private static void WriteLevelsRawData(byte[] levelsData)
        {
            File.WriteAllBytes(Constants.LevelRawDataPath, levelsData);
        }

        private static Color[] GetPaletteColors(int row)
        {
            byte[] row_palette = new byte[Constants.PALETTE_BLOCK_SIZE];

            Buffer.BlockCopy(Constants.FULL_PALETTE, row * Constants.PALETTE_BLOCK_SIZE, row_palette, 0, row_palette.Length);

            List<Color> cList = new List<Color>();

            for (int i = 0; i < row_palette.Length; i++)
            {
                cList.Add(Color.FromArgb(
                                            255,

                                            (int)((double)((row_palette[i] & Constants.RED_MASK) << Constants.RED_MASK_SHIFT) * Constants.RED_MASK_MULT),

                                            (int)((double)((row_palette[i] & Constants.GREEN_MASK) << Constants.GREEN_MASK_SHIFT) * Constants.GREEN_MASK_MULT),

                                            (int)((double)((row_palette[i] & Constants.BLUE_MASK) << Constants.BLUE_MASK_SHIFT) * Constants.BLUE_MASK_MULT)
                                        ));
            }
            return cList.ToArray();
        }

        private static void DrawLevelTile(Bitmap charBm, Graphics g, int srcX, int srcY, int destX, int destY)
        {
            g.DrawImage(charBm,
                        new Rectangle() { X = destX, Y = destY, Width = Constants.CharactersCharSize, Height = Constants.CharactersCharSize },
                        new Rectangle() { X = srcX, Y = srcY, Width = Constants.CharactersCharSize, Height = Constants.CharactersCharSize },
                        GraphicsUnit.Pixel);

        }

        private static void DrawEnemy(Bitmap spriteBm, Graphics g, int srcX, int srcY, int destX, int destY)
        {
            g.DrawImage(spriteBm,
                       new Rectangle() { X = destX, Y = destY, Width = Constants.SpriteCharSize, Height = Constants.SpriteCharSize },
                       new Rectangle() { X = srcX, Y = srcY, Width = Constants.SpriteCharSize, Height = Constants.SpriteCharSize },
                       GraphicsUnit.Pixel);
        }

        private static void DrawLevelTileContent(int level, Bitmap charBm, Graphics g, int srcY, int destX, int destY, int currentSkip, bool isUp)
        {
            int contentTile = GetTitleContentColor(level, isUp, currentSkip);
            if (isUp)
            {
                for (int i = destY - Constants.CharactersCharSize; i > 0x27; i -= 8)
                {
                    g.DrawImage(charBm,
                          new Rectangle() { X = destX, Y = i, Width = Constants.CharactersCharSize, Height = Constants.CharactersCharSize },
                          new Rectangle() { X = contentTile, Y = srcY, Width = Constants.CharactersCharSize, Height = Constants.CharactersCharSize },
                          GraphicsUnit.Pixel);
                }
            }
            else
            {
                for (int i = destY + Constants.CharactersCharSize; i < 0xF0; i += 8)
                {
                    g.DrawImage(charBm,
                          new Rectangle() { X = destX, Y = i, Width = Constants.CharactersCharSize, Height = Constants.CharactersCharSize },
                          new Rectangle() { X = contentTile, Y = srcY, Width = Constants.CharactersCharSize, Height = Constants.CharactersCharSize },
                          GraphicsUnit.Pixel);

                }

            }
        }

        private static int GetTitleContentColor(int level, bool isUp, int skipPosition)
        {
            int res = -1;
            switch (level)
            {
                case 1:
                    res = 0x39 * 8;
                    break;
                case 2:
                    res = 0x39 * 8;
                    break;
                case 3:
                    res = 0x39 * 8;
                    break;
                case 4:
                    if (isUp)
                    {
                        res = skipPosition % 2 == 0 ? 0xD0 * 8 : 0x3D * 8;
                    }
                    else
                    {
                        res = skipPosition % 2 == 0 ? 0x3D * 8 : 0xD0 * 8;

                    }

                    break;
                case 5:
                    if (isUp)
                    {
                        res = skipPosition % 2 == 0 ? 0xD0 * 8 : 0x3D * 8;
                    }
                    else
                    {
                        res = skipPosition % 2 == 0 ? 0x3D * 8 : 0xD0 * 8;

                    }
                    break;
                case 6:
                    res = skipPosition % 2 == 0 ? 0x3D * 8 : 0xD0 * 8;
                    break;
            }
            return res;
        }
    }
}
