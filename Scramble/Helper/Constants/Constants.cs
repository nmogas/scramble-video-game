﻿using System;
using System.IO;
namespace Scramble
{
    public class Constants
    {
        public static int SCREEN_HEIGHT = 256;

        public static int LEVEL_HEIGHT = 240;

        public static int SCREEN_WIDTH = 11200;

        public static int CHARACTER_PALETTE_INDEX = 2 * 8;

        public static int SPRITE_PALETTE_INDEX = 2 * 16;

        public static string Scramble = "./assets/rom/scramble";

        public static string CharactersPath = "./assets/rom/scramble.char.png";

        public static string SpritePath = "./assets/rom/scramble.sprite.png";

        public static string LevelRawDataPath = "./assets/rom/levels.raw.png";

        public static string PalettePath = "./assets/rom/c01s.6e";

        public static string GraphicsPart1Path = "./assets/rom/c2.5f";

        public static string GraphicsPart2Path = "./assets/rom/c1.5h";

        public static int CharactersCharSize = 8;

        public static int SpriteCharSize = 16;

        public static string[] DataPath = {
                                        "./assets/rom/s1.2d",
                                        "./assets/rom/s2.2e",
                                        "./assets/rom/s3.2f",
                                        "./assets/rom/s4.2h",
                                        "./assets/rom/s5.2j",
                                        "./assets/rom/s6.2l",
                                        "./assets/rom/s7.2m",
                                        "./assets/rom/s8.2p"
                                       };


        //BB GGG RRR
        public static int RED_MASK = 0x07;
        public static int RED_MASK_SHIFT = 5;
        //the highest value we could get was 224 (because of the shift (1110 0000)) - need to multiply it to get 255 if the case
        public static double RED_MASK_MULT = (double)0xFF / (double)(RED_MASK << RED_MASK_SHIFT);// 224.0; // max / (0x07 << 5)

        public static int GREEN_MASK = 0x38;
        public static int GREEN_MASK_SHIFT = 2;
        public static double GREEN_MASK_MULT = (double)0xFF / (double)(GREEN_MASK << GREEN_MASK_SHIFT); // max / (0x38 << 2)

        public static int BLUE_MASK = 0xC0;
        public static int BLUE_MASK_SHIFT = 0;
        public static double BLUE_MASK_MULT = (double)0xFF / (double)(BLUE_MASK << BLUE_MASK_SHIFT); //max / (0xc0 << 0)

        public static int PALETTE_BLOCK_SIZE = 4;

        public static byte[] FULL_PALETTE = File.ReadAllBytes(Constants.PalettePath);

        /* convert byte memory index to int */
        public static int DATA_START_INDEX = Convert.ToInt32("0x29E2", 16);

        /* convert byte memory index to int */
        public static int DATA_END_INDEX = Convert.ToInt32("0x3CEF", 16);

        public const int ROCKETS = 0x1C0;

        public const int FUEL = 0x100;

        public const int DROID = 0x330;

        public const int END_OBJECT = 0x1F0;

    }
}
