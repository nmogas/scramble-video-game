# Scramble #

"Scramble (スクランブル Sukuranburu?) is a 1981 side-scrolling shoot 'em up arcade game. It was developed by Konami, and manufactured and distributed by Leijac in Japan and Stern in North America. It was the first side-scrolling shooter with forced scrolling and multiple distinct levels.The Konami Scramble arcade system board hardware uses two Zilog Z80 microprocessors for the central processing unit, two AY-3-8910 sound chips for the sound, and Namco Galaxian video hardware for the graphics."

## Goal ##

Do an - exact copy - of the old arcade game in Javascript. This includes exploring the original ROM behavior, extracting tiles/sprites as well as the logic. Ultimately you will be able to run this game in any browser without any dependencies (build in pure javascript - no fancy libraries like jquery ).

## Building ##

## Running ##

## Special Thanks ##
Sam Skivington