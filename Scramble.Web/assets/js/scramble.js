// Scramble js remake
var WRAPPER_ID = "scrambleWrapper";
var SCRAMBLE_ICON_ID = "scrambleIcon";

var TILE_FILE_PATH = "assets/img/scramble.char.png";
var LAYOUT_OBJECT_FILE_PATH = "assets/img/scramble.sprite.png";
var LEVELS_FILE_PATH = "assets/data/levels.raw.png";

//list of characters available in the characters .png with their positions
var CHARACTERS_STRING = "0123456789000000 ABCDEFGHIJKLMNOPQRSTUVWXYZ-";

//this characters are in weird positions
var SPECIAL_CHARACTERS = {
    COPYRIGHT: 62 * 8,
    QUESTION_MARK: 210 * 8,
    DOT: 145 * 8
}

//number of pixel use by each char / image on respectig images
var CHARACTER_SIZE = 8;
var PALETTE_N_ROWS = 8;
var TILE_SIZE = 16;

var SCREEN_WIDTH = 224; //default 224
var SCREEN_HEIGHT = 256; //default 256;
var GAME_FPS = 60; //draw 60 times per second

//init messages
var LOAD_MESSAGES = {
    CHAR_OK: {
        message: "CHARACTERS  OK",
        xPos: 16,
        yPos: 16,
        paletteIdx: 1
    },
    ROM_FAIL: {
        message: "ROM  FAIL",
        xPos: 72,
        yPos: 32,
        paletteIdx: 3
    },
    ROM_OK: {
        message: "ROM  OK",
        xPos: 72,
        yPos: 32,
        paletteIdx: 1
    },
};


//set x padding in certain screens 
var FIXED_RIGHT_xPOS = SCREEN_WIDTH - (6 * CHARACTER_SIZE);
var FIXED_LEFT_xPOS = 6 * CHARACTER_SIZE;

//attract mode messages
var ATTRACT_MODE_MESSAGE = {
    HEADER_FOOTER: {
        message: ["1UP    HIGH SCORE", "00      10000", "CREDIT  0"],
        xPos: [CHARACTER_SIZE * 3, CHARACTER_SIZE * 5, CHARACTER_SIZE * 2],
        yPos: [0, CHARACTER_SIZE, SCREEN_HEIGHT - CHARACTER_SIZE],
        paletteIdx: [0, 1, 6]
    },

    HOW_FAR: {
        message: ["PLAY", "- SCRAMBLE -", "HOW FAR CAN YOU INVADE", "OUR SCRAMBLE SYSTEM ?", "� KONAMI  1981"],
        xPos: [(SCREEN_WIDTH - (4 * CHARACTER_SIZE)) / 2, (SCREEN_WIDTH - (12 * CHARACTER_SIZE)) / 2, (SCREEN_WIDTH - (22 * CHARACTER_SIZE)) / 2, (SCREEN_WIDTH - (21 * CHARACTER_SIZE)) / 2, FIXED_RIGHT_xPOS - (14 * CHARACTER_SIZE)],
        yPos: [7 * CHARACTER_SIZE, 9 * CHARACTER_SIZE, SCREEN_HEIGHT / 2 + CHARACTER_SIZE, SCREEN_HEIGHT / 2 + 4 * CHARACTER_SIZE, SCREEN_HEIGHT - 4 * CHARACTER_SIZE],
        paletteIdx: [1, 6, 3, 3, 0]
    },
    RANKING: {
        message: ["- SCORE RANKING -", "� KONAMI  1981"],
        xPos: [(SCREEN_WIDTH - (16 * CHARACTER_SIZE)) / 2, FIXED_RIGHT_xPOS - (14 * CHARACTER_SIZE)],
        yPos: [4 * CHARACTER_SIZE, SCREEN_HEIGHT - 4 * CHARACTER_SIZE],
        paletteIdx: [3, 6]
    },
    POINTS: {
        message: ["- SCORE TABLE -", "� KONAMI  1981"],
        xPos: [(SCREEN_WIDTH - (13 * CHARACTER_SIZE)) / 2, FIXED_RIGHT_xPOS - (14 * CHARACTER_SIZE)],
        yPos: [7 * CHARACTER_SIZE, SCREEN_HEIGHT - 4 * CHARACTER_SIZE],
        paletteIdx: [1, 0]
    }
};
//attract mode scores / points values
var ATTRACT_MODE_POINTS = {
    RANKING: {
        rank: ["1ST", "2ND", "3RD", "4TH", "5TH", "6TH", "7TH", "8TH", "9TH", "10TH"],
        color: [1, 1, 1, 6, 6, 6, 7, 7, 7, 7],
        points: "100000 PTS"
    },
    POINTS: {
        object: [448, 480, 416, 256, 608, 816],
        points: ["...  50 PTS", "...  80 PTS", "... 100 PTS", "... 150 PTS", "... 800 PTS", "... MISTERY"],
        paletteIdx: [0, 0, 6, 4, 5, 1]
    }
}

//types of ajax responses available
var AJAX_RESPONSE_TYPE = {
    ARRAY_BUFFER: "arraybuffer"
}

//types of possible ajax requests strings
var AJAX_REQUEST_TYPE = {
    GET: "GET",
    POST: "POST"
}

//http://www.javascripter.net/faq/keycodes.htm
//key codes
var KEY_LEFT_ARROW_CODE = 37;
var KEY_UP_ARROW_CODE = 38;
var KEY_RIGHT_ARROW_CODE = 39;
var KEY_DOWN_ARROW_CODE = 40;
var KEY_PAUSE_CODE = 80; // P key

//initializations countdowns
var INIT_SCREEN_COUNTDOWN = 10;
var WAIT_CPU_RESET_COUNTDOWN = 20;
var WAIT_CHARACTERS_COUNTDOWN = 30;
var WAIT_ALL_ASSETS_COUNTDOWN = 50;
var FINISH_TEST_COUNTDOWN = 50;
var FAIL_COUNTDOWN = 100;

//attract mode counters
var ATTRACT_MODE_INIT_COUNTDOWN = 120;
var ATTRACT_MODE_HOWFAR_COUNTDOWN = 100;
var ATTRACT_MODE_RANKING_COUNTDOWN = 100;
var ATTRACT_MODE_POINTS_COUNTDOWN = 380;
var ATTRACT_MODE_PLAYING_COUNTDOWN = 100;


//machine loop controls whether you're in startup mode, attract mode or playing mode.
var MACHINE_STATE = {
    SELF_TEST_MODE: 0,
    ATTRACT_MODE: 1,
    PLAY_MODE: 2
};

var PLAY_STATE = {

};

var SELF_TEST_MODE_STATE = {
    FAILED: -1,
    INIT_SCREEN: 0,
    WAIT_CPU_RESET: 1,
    WAIT_CHARACTERS: 2,
    WAIT_ALL_ASSETS: 3,
    FINISH_TEST: 4
};

var ATTRACT_MODE = {
    INIT: 0,
    HOW_FAR: 1,
    RANKING: 2,
    POINTS: 3,
    PLAYING: 4
};

var ASSET = {
    CHARACTERS: 0,
    LAYOUT_OBJECT: 1,
    LEVEL_DATA: 2
};

var ajax = {
    call: function (fileSrc, requestType, responseType, callback, asset) {
        if (fileSrc != null && requestType != null) {
            var xhr = new XMLHttpRequest;

            xhr.open(requestType, fileSrc, true);

            if (responseType != null) {
                xhr.responseType = responseType;
            }
            xhr.onload = function () {
                var arrayBuffer = this.response;
                callback(new Uint8Array(arrayBuffer), asset);
            }

            xhr.send(null);
        }
    }
}

var scramble = {
    //konami code conbination entered: triggers the game
    konamiEntered: false,

    //number of assets loaded so far
    assetsSucessLoaded: 0,

    //number of assets tried to load (sucess / unsuccess)
    assetsCount: 0,

    //tell us if any asset failed to load
    assetsFailed: false,

    //hold canvas reference
    canvas: null,

    //hold context reference
    ctx: null,

    //hold assets after being loaded
    assets: [],

    //fps time interval
    fpsTimer: null,

    // machine states
    machineState: 0,

    attractModeState: 0,

    //game states
    playModeState: 0,

    //test mode states
    selfTestModeState: null,

    //set countdown timer to wait between states
    selfModeCountdown: 0,

    //set attract mode countdown timer
    attractModeCountdown: 0,
    //how many points drawn so far
    attractModePointsDrawed: 0,
    //interval of drawing
    attractModeDrawPointsSpeed: -1,
    attractModePointsStartDrawingTime: -1,

    //game init
    Init: function () {
        if (scramble.context == null) {
            //set up canvas properties                    
            scramble.canvas = document.getElementById('scramble');
            scramble.canvas.width = SCREEN_WIDTH;
            scramble.canvas.height = SCREEN_HEIGHT;
            //set up context properties
            scramble.ctx = scramble.canvas.getContext('2d');
            scramble.ctx.mozImageSmoothingEnabled = false;
            scramble.ctx.webkitImageSmoothingEnabled = false;
        }
        if ((scramble.ctx != undefined) && (scramble.ctx != null)) {
            //set the machine state to its defaults - testing
            scramble.machineState = MACHINE_STATE.SELF_TEST_MODE;

            //test mode      
            scramble.selfTestModeState = SELF_TEST_MODE_STATE.INIT_SCREEN;
            scramble.selfModeCountdown = INIT_SCREEN_COUNTDOWN;

            //attract mode
            scramble.attractModeState = ATTRACT_MODE.INIT;
            scramble.attractModeCountdown = ATTRACT_MODE_INIT_COUNTDOWN;
            scramble.attractModePointsDrawed = 0;
            scramble.attractModePointsStartDrawingTime = ATTRACT_MODE_POINTS_COUNTDOWN - (ATTRACT_MODE_POINTS_COUNTDOWN / 4);
            scramble.attractModeDrawPointsSpeed = Math.floor(scramble.attractModePointsStartDrawingTime / ATTRACT_MODE_POINTS.POINTS.points.length);

            //game mode
            scramble.playModeState = ""; //TODO

            // start the screen renderer
            scramble.Restart();

            //start loading the game's assets 
            scramble.LoadAssets();
        } else {
            //should never get here
            scramble.Stop();
        }
    },

    Stop: function () {
        //sets konami as not entered
        scramble.codeEntered = false;

        //remove timer from canvas
        scramble.CancelTimer();

        //hide block
        document.getElementById(WRAPPER_ID).style.display = "none";

        //show icon
        document.getElementById(SCRAMBLE_ICON_ID).style.display = "";

        return false;
    },

    Update: function () {
        //update the current machine state every "scramble.fpsTimer"        
        switch (scramble.machineState) {
            case MACHINE_STATE.SELF_TEST_MODE:
                //update the test mode internal states
                scramble.UpdateTestMode();
                break;
            case MACHINE_STATE.ATTRACT_MODE:
                //update the attract mode states
                scramble.UpdateAttractMode();
                break;
            case MACHINE_STATE.PLAY_MODE:
                //update play mode (runnning) internal states
                scramble.UpdatePlayMode();
                break;
        }
    },

    //update test mode
    UpdateTestMode: function () {
        //make sure the test mode first state was set up
        if ((scramble.selfTestModeState != undefined) && (scramble.selfTestModeState != null)) {
            //wait for countdown to finish            
            if (scramble.selfModeCountdown != 0) {
                scramble.selfModeCountdown--;
            } else {
                //do things based on state :P
                switch (scramble.selfTestModeState) {
                    case SELF_TEST_MODE_STATE.INIT_SCREEN:
                        //clear the screen
                        scramble.ClearScreen();

                        //set delay for characters load
                        scramble.selfModeCountdown = WAIT_CPU_RESET_COUNTDOWN;

                        //switch state to wait_cpu_reset
                        scramble.selfTestModeState = SELF_TEST_MODE_STATE.WAIT_CPU_RESET;
                        break;

                    case SELF_TEST_MODE_STATE.WAIT_CPU_RESET:
                        //wait for characters assets to load
                        scramble.selfModeCountdown = WAIT_CHARACTERS_COUNTDOWN;

                        //switch state to wait_characters
                        scramble.selfTestModeState = SELF_TEST_MODE_STATE.WAIT_CHARACTERS;
                        break;

                    case SELF_TEST_MODE_STATE.WAIT_CHARACTERS:
                        //waits until asset[0] is loaded (characters) then switch to wait_all_assets
                        if ((scramble.assets[ASSET.CHARACTERS].loaded != undefined) && (scramble.assets[ASSET.CHARACTERS].loaded == true)) {
                            //characters are loaded, wait for rest of assets to load
                            scramble.selfModeCountdown = WAIT_ALL_ASSETS_COUNTDOWN;
                            //print notification message
                            scramble.WriteText(LOAD_MESSAGES.CHAR_OK.xPos, LOAD_MESSAGES.CHAR_OK.yPos, LOAD_MESSAGES.CHAR_OK.message, LOAD_MESSAGES.CHAR_OK.paletteIdx);
                            //wait for all assets
                            scramble.selfTestModeState = SELF_TEST_MODE_STATE.WAIT_ALL_ASSETS;
                        } else {
                            //characters should be loaded by now... fail...                            
                            scramble.selfTestModeState = SELF_TEST_MODE_STATE.FAILED;
                        }
                        break;

                    case SELF_TEST_MODE_STATE.WAIT_ALL_ASSETS:
                        //can draw text now because characters are already loaded
                        if (scramble.assetsSucessLoaded != scramble.assetsCount) {
                            //failed
                            scramble.selfModeCountdown = FAIL_COUNTDOWN;
                            scramble.selfTestModeState = SELF_TEST_MODE_STATE.FAILED;
                            scramble.WriteText(LOAD_MESSAGES.ROM_FAIL.xPos, LOAD_MESSAGES.ROM_FAIL.yPos, LOAD_MESSAGES.ROM_FAIL.message, LOAD_MESSAGES.ROM_FAIL.paletteIdx);

                        } else {
                            scramble.WriteText(LOAD_MESSAGES.ROM_OK.xPos, LOAD_MESSAGES.ROM_OK.yPos, LOAD_MESSAGES.ROM_OK.message, LOAD_MESSAGES.ROM_OK.paletteIdx);
                            scramble.selfModeCountdown = FINISH_TEST_COUNTDOWN;
                            //change to finish_test                                        
                            scramble.selfTestModeState = SELF_TEST_MODE_STATE.FINISH_TEST;
                        }
                        break;

                    case SELF_TEST_MODE_STATE.FINISH_TEST:
                        //change machine state to attract mode                        
                        scramble.machineState = MACHINE_STATE.ATTRACT_MODE;
                        break;

                    case SELF_TEST_MODE_STATE.FAILED:
                        //stop timer and exit
                        scramble.CancelTimer();
                        scramble.Stop();
                        break;
                }
            }
        } else {
            return false;
        }
    },

    //update attract mode
    UpdateAttractMode: function () {
        if (scramble.attractModeState != undefined && scramble.attractModeState != null) {
            //wait for countdown to finish and keep drawing things           
            if (scramble.attractModeCountdown != 0) {
                scramble.attractModeCountdown--;
                //if not reach 0 -> do not change state and keep updating the screen
                if (scramble.attractModeCountdown == 0) {
                    switch (scramble.attractModeState) {
                        case ATTRACT_MODE.INIT:
                            //change to how far intro menu menu
                            scramble.attractModeState = ATTRACT_MODE.HOW_FAR;

                            //reset timer
                            scramble.attractModeCountdown = ATTRACT_MODE_HOWFAR_COUNTDOWN;
                            break;
                        case ATTRACT_MODE.HOW_FAR:
                            //change to rank menu
                            scramble.attractModeState = ATTRACT_MODE.RANKING;

                            //reset timer
                            scramble.attractModeCountdown = ATTRACT_MODE_RANKING_COUNTDOWN;
                            break;

                        case ATTRACT_MODE.RANKING:
                            //change to points menu
                            scramble.attractModeState = ATTRACT_MODE.POINTS;

                            //set back to 0 the drawed points
                            scramble.attractModePointsDrawed = 0;

                            //reset timer
                            scramble.attractModeCountdown = ATTRACT_MODE_POINTS_COUNTDOWN;
                            break;

                        case ATTRACT_MODE.POINTS:
                            //change to attract mode player
                            scramble.attractModeState = ATTRACT_MODE.PLAYING;

                            //reset timer
                            scramble.attractModeCountdown = ATTRACT_MODE_PLAYING_COUNTDOWN;
                            break;

                        case ATTRACT_MODE.PLAYING:
                            //repeat to the first attract mode screen
                            scramble.attractModeState = ATTRACT_MODE.HOW_FAR;

                            //reset timer
                            scramble.attractModeCountdown = ATTRACT_MODE_HOWFAR_COUNTDOWN;
                            break;
                    }
                    scramble.DrawAttractMode();
                } else {
                    scramble.DrawAttractMode();
                }
            } else {
                scramble.DrawAttractMode();
            }
        }
    },

    DrawAttractMode: function () {
        if (scramble.attractModeState != undefined && scramble.attractModeState != null) {
            //clear screen
            scramble.ClearScreen();

            //init attract mode - should square matrix and slowly remove it
            if (scramble.attractModeState == ATTRACT_MODE.INIT) {
                //draw square matrix                 
                for (var i = 0; i <= SCREEN_WIDTH; i += CHARACTER_SIZE) {
                    for (var j = 0; j < SCREEN_HEIGHT; j += CHARACTER_SIZE) {
                        scramble.WriteTile(scramble.assets[ASSET.CHARACTERS], 504, 0, CHARACTER_SIZE, CHARACTER_SIZE, i, j, CHARACTER_SIZE, CHARACTER_SIZE);
                    }
                }
                //countdown is reaching the end -> show erase effect
                if (scramble.attractModeCountdown == 1) {
                    scramble.ClearScreenEffect(SCREEN_WIDTH, 0, 0, SCREEN_HEIGHT, ATTRACT_MODE_INIT_COUNTDOWN / 4);
                }
            } else {

                //to make easy to read - ALWAYS DRAW HEADER (common across all screens (despite init))
                var h = ATTRACT_MODE_MESSAGE.HEADER_FOOTER;
                for (var i = 0; i < h.message.length; i++) {
                    scramble.WriteText(h.xPos[i], h.yPos[i], h.message[i], h.paletteIdx[i]);
                }

                //holds the message according to the attract mode state and make it easy to read                
                var m;
                switch (scramble.attractModeState) {
                    case ATTRACT_MODE.HOW_FAR:
                        m = ATTRACT_MODE_MESSAGE.HOW_FAR;
                        break;
                    case ATTRACT_MODE.RANKING:
                        m = ATTRACT_MODE_MESSAGE.RANKING;
                        r = ATTRACT_MODE_POINTS.RANKING; //get the ranking table
                        for (var i = 0 ; i < r.rank.length; i++) {
                            //draw rank - fixed x position - use ranking message as starting point for Y coordinate
                            scramble.WriteText(FIXED_LEFT_xPOS, m.yPos[0] + 3 * CHARACTER_SIZE + (i * CHARACTER_SIZE * 2), r.rank[i], r.color[i]);
                            //draw point - fixed x position - use ranking message as starting point for Y coordinate
                            scramble.WriteText(FIXED_RIGHT_xPOS - r.points.length * CHARACTER_SIZE, m.yPos[0] + 3 * CHARACTER_SIZE + (i * CHARACTER_SIZE * 2), r.points, r.color[i]);
                        }
                        //last case
                        if (scramble.attractModeCountdown == 1) {
                            //erase content with style!
                            //erase from title to konami footer
                            scramble.ClearScreenEffect(SCREEN_WIDTH, m.yPos[0], 0, m.yPos[1] - m.yPos[0], 30);
                        }
                        break;

                    case ATTRACT_MODE.POINTS:
                        m = ATTRACT_MODE_MESSAGE.POINTS;
                        p = ATTRACT_MODE_POINTS.POINTS; //get the points table
                        for (var i = 0 ; i < p.points.length; i++) {
                            //draw game enemies / objects                            
                            scramble.WriteTile(scramble.assets[ASSET.LAYOUT_OBJECT], p.object[i], p.paletteIdx[i] * TILE_SIZE, TILE_SIZE, TILE_SIZE, FIXED_LEFT_xPOS + 2 * CHARACTER_SIZE, m.yPos[0] + TILE_SIZE + i * (TILE_SIZE + CHARACTER_SIZE), TILE_SIZE, TILE_SIZE);
                        }

                        if (scramble.attractModeCountdown <= scramble.attractModePointsStartDrawingTime) {
                            //check which point is drawing
                            if (scramble.attractModeCountdown % scramble.attractModeDrawPointsSpeed == 0) {
                                scramble.attractModePointsDrawed++;
                            }
                            //start drawing letter by letter
                            for (var i = 0 ; i < scramble.attractModePointsDrawed ; i++) {
                                //starting point "score table" - points objects
                                scramble.WriteText(FIXED_RIGHT_xPOS - p.points[i].length * CHARACTER_SIZE, m.yPos[0] + TILE_SIZE + i * (TILE_SIZE + CHARACTER_SIZE) + CHARACTER_SIZE / 2, p.points[i], 0);
                            }
                        }                        
                        break;

                    case ATTRACT_MODE.PLAYING:
                        //TODO
                        scramble.WriteText((SCREEN_WIDTH - (16 * CHARACTER_SIZE)) / 2, SCREEN_HEIGHT / 2, "SOME ACTION NO??", 6);
                        break;
                }
                if (m != undefined) {
                    for (var i = 0; i < m.message.length; i++) {
                        scramble.WriteText(m.xPos[i], m.yPos[i], m.message[i], m.paletteIdx[i]);
                    }
                }
            }
        }
    },

    //update play mode
    UpdatePlayMode: function () {
        if (scramble.playModeState != undefined && scramble.playModeState != null) {
            switch (scramble.playModeState) {
                //TODO
                //case PLAY_STATE.SOMETHING:
                //    break;
            }
        }
    },


    //write text on screen - users characters string and then maches with the chars.png position
    WriteText: function (xPos, yPos, text, paletteIdx) {
        if (paletteIdx >= 0 && paletteIdx <= PALETTE_N_ROWS) {
            var characterText;
            for (var i = 0; i < text.length; i++) {
                var newYPos = yPos; //THIS IS BAD!
                if (text[i].charCodeAt() == '65533') {
                    characterText = SPECIAL_CHARACTERS.COPYRIGHT;
                } else if (text[i] == '?') {
                    characterText = SPECIAL_CHARACTERS.QUESTION_MARK;
                } else if (text[i] == '.') {
                    characterText = SPECIAL_CHARACTERS.DOT;
                } else {
                    characterText = CHARACTERS_STRING.indexOf(text[i].toUpperCase()) * 8;
                }

                if (characterText == SPECIAL_CHARACTERS.DOT && scramble.attractModeState == ATTRACT_MODE.POINTS) {
                    //THIS IS BAD!!!!!!! VERY BAD! THIS FUNCTION SHOULD JUST PRINT BASED ON THE PARAMETERS AND NOT HAVE SPECIAL CASES!
                    newYPos = yPos + 4;
                }
                scramble.ctx.drawImage(scramble.assets[ASSET.CHARACTERS],
                                        characterText,
                                        paletteIdx * CHARACTER_SIZE,
                                        CHARACTER_SIZE,
                                        CHARACTER_SIZE,
                                        xPos + (i * CHARACTER_SIZE),
                                        newYPos,
                                        CHARACTER_SIZE,
                                        CHARACTER_SIZE);
            }
        }
    },

    //cut a tile from the images and draw it 
    WriteTile: function (img, srcX, paletteIdx, srcWidth, srcHeight, destX, destY, destWidth, destHeight) {
        scramble.ctx.drawImage(img, srcX, paletteIdx, srcWidth, srcHeight, destX, destY, destWidth, destHeight);
    },

    //clear screen with window effect
    ClearScreenEffect: function (startXPos, startYPos, endxPos, endYPos, speed) {
        var i = 0;
        //stop timer
        scramble.CancelTimer();
        scramble.fpsTimer = setInterval(function () {
            scramble.ctx.fillRect(startXPos + (i * CHARACTER_SIZE), startYPos, startXPos + (i * CHARACTER_SIZE) + CHARACTER_SIZE, endYPos);
            if (startXPos < endxPos) {
                i++;
            } else {
                i--;
            }
            if (startXPos + (i * CHARACTER_SIZE) == endxPos) {
                //get back to the main timer
                scramble.Restart();
            }
        }, speed);
    },

    ClearScreen: function () {
        //set background color, for instance, to something off-black                  
        scramble.ctx.fillStyle = "#2a2a2a";
        scramble.ctx.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    },

    CancelTimer: function () {
        // stop and free the timer        
        clearInterval(scramble.fpsTimer);
        scramble.fpsTimer = null;
    },

    Restart: function () {
        //stop and free the timer - start again aftar
        scramble.CancelTimer();
        scramble.fpsTimer = setInterval(scramble.Update, 1000 / GAME_FPS); // this should be enough to 60fps        
    },


    //start to load the assets
    LoadAssets: function () {
        scramble.LoadImageFile(TILE_FILE_PATH, ASSET.CHARACTERS);
        scramble.LoadImageFile(LAYOUT_OBJECT_FILE_PATH, ASSET.LAYOUT_OBJECT);
        scramble.LoadDataFile(LEVELS_FILE_PATH, ASSET.LEVEL_DATA);
    },

    //load the asset an image
    LoadImageFile: function (filePath, asset) {
        var img = new Image();
        img.onload = scramble.PreloadFileOnLoad;
        img.onerror = scramble.PreloadFileOnError;
        img.src = filePath;
        scramble.assets[asset] = img;
        scramble.assetsCount++;
    },

    //load the asset using an async ajax call as a byte data
    LoadDataFile: function (filePath, asset) {
        ajax.call(filePath, AJAX_REQUEST_TYPE.GET, AJAX_RESPONSE_TYPE.ARRAY_BUFFER, scramble.PreLoadDataCallback, asset);
    },

    //onload file error
    PreloadFileOnError: function () {
        console.log("An asset failed to load: " + this.src);
        scramble.assetsFailed = true;
        this.loaded = false;
    },

    //onload file assets update
    PreloadFileOnLoad: function () {
        scramble.assetsSucessLoaded++;
        this.loaded = true;
    },

    //data assets callback function
    PreLoadDataCallback: function (data, idx) {
        //assets was loaded or not - count anyway
        scramble.assetsCount++;
        if (data) {
            scramble.assets[idx] = data;
            scramble.assets[idx].loaded = true;
            //sucessful loaded
            scramble.assetsSucessLoaded++;
        } else {
            console.log("An asset failed to load: " + idx);
            scramble.assets[idx].loaded = false;
            scramble.assetsFailed = true;
        }
    },

    //the game can only be triggered if the konami code has been entered: TODO
    KonamiTriggerAction: function () {
        //mark konami as entered
        scramble.konamiEntered = true;

        //init the game data and display window
        scramble.Init();

        //show canvas
        document.getElementById(WRAPPER_ID).style.display = "block";
        document.getElementById(WRAPPER_ID).focus();

        //hide mini rocket icon
        document.getElementById(SCRAMBLE_ICON_ID).style.display = "none";

        return false;
    },

}

//pause event - game not selected
window.addEventListener('blur', function () {
    if (scramble.konamiEntered) {
        scramble.CancelTimer();
    }
}, false);

//focus event - game is focused - restart time
window.addEventListener('focus', function () {
    if (scramble.konamiEntered) {
        scramble.Restart();
    }
}, false);

//check when a key is pressed 
window.addEventListener('keydown', function (event) {
    //make only available if the game is running

}, false);

//check when a key is released
window.addEventListener('keyup', function (event) {
}, false);
